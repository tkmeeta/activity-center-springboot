package com.ybf.activity.web.test.controller;

import com.ybf.activity.web.entity.Student;
import com.ybf.activity.web.test.base.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by Administrator on 2017/7/13.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestControllerTest {
    private final static String baseUrl = "http://localhost:";
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testZh() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(baseUrl + port + "/activity-web/test/zh",String.class);
        assertThat(response.getBody(), equalTo("你好"));
    }

    @Test
    public void testStudent() {
        String url = baseUrl + port + "/activity-web/student/{id}";
        ResponseEntity<Student> response = testRestTemplate.getForEntity(url,Student.class,1);
        Student student = response.getBody();
        assertThat(student,notNullValue());

        System.out.println(student);
    }
}

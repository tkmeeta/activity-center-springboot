package com.ybf.activity.web.test.mapper;

import com.github.pagehelper.PageHelper;
import com.ybf.activity.web.entity.Student;
import com.ybf.activity.web.mapper.StudentMapper;
import com.ybf.activity.web.test.base.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 2017/7/10.
 */
public class StudentMapperTest extends BaseTest {

    @Autowired
    private StudentMapper sm;

    @Test
    public void testGetById() {
        Student stu = sm.getById(1);
        assert null != stu;
    }

    @Test
    public void testSelectByPage() {
        PageHelper.startPage(1,3);
        List<Student> students = sm.sel();
        assert students.size() > 0;
    }
}

package com.ybf.activity.web.test.base;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2017/7/10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.ybf.activity.web.Application.class)
public abstract class BaseTest {
}

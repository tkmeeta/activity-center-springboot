package com.ybf.activity.web.test.jms;

import com.ybf.activity.web.jms.demo.JMSProducer;
import com.ybf.activity.web.test.base.BaseTest;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * Created by Administrator on 2017/7/12.
 */
public class JmsTest extends BaseTest{
    @Autowired
    private JMSProducer jmsProducer;
    @Autowired
    private Topic topic;
    @Autowired
    private Queue queue;

    @Test
    public void testJms() {
        for (int i=0;i<10;i++) {
            jmsProducer.sendMessage(queue,"queue,world!" + i);
            jmsProducer.sendMessage(topic,"topic,world!" + i);
        }
    }
}

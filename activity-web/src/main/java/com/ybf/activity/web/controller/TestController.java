package com.ybf.activity.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2017/7/12.
 */
@Controller
@RequestMapping("/test")
public class TestController {
    protected static final Logger logger = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping(value = "/zh",method = RequestMethod.GET)
    @ResponseBody
    public String zhTest() {
        logger.info("你访问的方法是：zhTest()");
        return "你好";
    }

    @RequestMapping(value = "/redis/{key}/{value}",method = RequestMethod.GET)
    @ResponseBody
    public String redisTest(@PathVariable String key,@PathVariable String value) {
        String redisValue = stringRedisTemplate.opsForValue().get(key);
        if (StringUtils.isEmpty(redisValue)) {
            stringRedisTemplate.opsForValue().set(key,value);
            return "操作成功！";
        }

        if (!redisValue.equals(value)) {
            stringRedisTemplate.opsForValue().set(key,value);
            return "操作成功！";
        }
        return String.format("redis中已存在[key=%s,value=%s]的数据！",key,value);
    }
}

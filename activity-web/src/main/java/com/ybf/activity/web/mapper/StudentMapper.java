package com.ybf.activity.web.mapper;

import com.ybf.activity.web.entity.Student;
import com.ybf.activity.web.config.mybatis.BaseMapper;

import java.util.List;

/**
 * Created by Administrator on 2017/7/7.
 */
public interface StudentMapper extends BaseMapper{
    Student getById(int id);
    List<Student> sel();
}

package com.ybf.activity.web.controller;

import com.github.pagehelper.PageHelper;
import com.ybf.activity.web.entity.Student;
import com.ybf.activity.web.entity.User;
import com.ybf.activity.web.mapper.StudentMapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/6.
 */
@Controller
public class IndexController {
    protected static final Logger logger = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    StudentMapper studentMapper;

    @Value("${application.title:活动中心后台系统}")
    private String title;

    @ApiIgnore
    @RequestMapping(value = {"/index"})
    public ModelAndView index() throws UnsupportedEncodingException {
        ModelAndView mv =new ModelAndView();
        mv.setViewName("/greeting");
        mv.addObject("title",title);

        logger.info("访问index(),title={}",title);

        // 测试list
        List<User> users = new ArrayList<>(5);
        for (int i=0;i<5;i++) {
            User user = new User();
            user.setId(i + 1);
            user.setName("测试用户"+(i+1));
            user.setSalary(1000000*Math.random());
            users.add(user);
        }

        mv.addObject("users",users);
        mv.addObject("zh","你好");
        return mv;
    }

    @ApiOperation(value = "用户某个用户的信息",notes = "根据用户id获取用户详细信息")
    @ApiImplicitParam(name = "id",value = "用户id",required = true,dataType = "int",paramType = "path")
    @RequestMapping(value = "/student/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Student student(@PathVariable int id) {
        return studentMapper.getById(id);
    }

    @ApiOperation(value = "获取用户信息（分页）",notes = "根据传入的页数获取用户信息")
    @ApiImplicitParam(name="pageNo",value = "页数",required = true,dataType = "int",paramType = "path")
    @RequestMapping(value = "/student/page/{pageNo}",method = RequestMethod.GET)
    @ResponseBody
    public List<Student> selectStudentByPage(@PathVariable int pageNo) {
        if (pageNo > 0) {
            PageHelper.startPage(pageNo,3); // 设置分页，参数1=页数，参数2=每页显示条数
        }
        return studentMapper.sel();
    }

    @ApiOperation(value = "中文异常测试",notes = "中文异常信息测试")
    @RequestMapping(value = "/exception",method = RequestMethod.GET)
    public String exceptionTest() throws Exception {
        throw new Exception("中文异常");
    }

    @ApiOperation(value = "JSON中文测试",notes = "根据用户输入的内容响应给用户，用来测试中文")
    @ApiImplicitParam(name = "msg",value = "消息内容",required = true,dataType = "String",paramType = "path")
    @RequestMapping(value = "/charset/{msg}",method = RequestMethod.GET)
    @ResponseBody
    public String charset(@PathVariable String msg) {
        logger.info("您输入的内容是：{}",msg);

        return msg;
    }

    @ApiOperation(value = "测试",notes = "JSON测试")
    @RequestMapping(value = "/test",method = RequestMethod.GET)
    @ResponseBody
    public Map test() {
        Map map = new HashMap<>(2);
        map.put("id",10001);
        map.put("name","你好,Spring boot!");
        return map;
    }
}
